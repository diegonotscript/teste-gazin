import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn, BeforeInsert } from "typeorm";
import { hashSync } from 'bcrypt'
@Entity('users')
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string
    @Column('varchar', { nullable: false })
    nome: string
    @Column('int', { nullable: false })
    idade: number
    @Column('varchar', { array: true, nullable: false })
    hobbys: string[]
    @Column('varchar', { nullable: false })
    nascimento: string
    @Column('varchar', { nullable: false, unique: true })
    email: string
    @Column('varchar', { nullable: false })
    password: string
    @CreateDateColumn({ name: 'created_at', nullable: true })
    createdAt: string
    @UpdateDateColumn({ name: 'updated_at', nullable: true })
    updatedAt: string

    @BeforeInsert()
    hashPassword() {
        this.password = hashSync(this.password, 10);
    }
}