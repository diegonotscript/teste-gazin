import { IsEmail, IsNotEmpty } from 'class-validator';

export class CreateUserDto {
    @IsNotEmpty()
    nome: string;

    @IsNotEmpty()
    idade: number
    @IsNotEmpty()
    hobbys: string[];
    @IsNotEmpty()
    nascimento: string
    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    password: string;
}
