FROM node as base

WORKDIR /monorepo

ENV PATH /monorepo/node_modules/.bin:$PATH

COPY ["package.json","yarn.lock","lerna.json","./"]

FROM base as development 

COPY ./packages/backend/ ./packages/backend

COPY ./packages/web/ ./packages/web

EXPOSE 3000
EXPOSE 4200


